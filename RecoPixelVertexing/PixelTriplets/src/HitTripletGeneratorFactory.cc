#include "RecoPixelVertexing/PixelTriplets/interface/HitTripletGeneratorFactory.h"

#include "FWCore/PluginManager/interface/PluginFactory.h"
 
EDM_REGISTER_PLUGINFACTORY(HitTripletGeneratorFactory,"HitTripletGeneratorFactory");
