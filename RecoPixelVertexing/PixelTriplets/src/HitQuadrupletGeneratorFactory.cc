#include "RecoPixelVertexing/PixelTriplets/interface/HitQuadrupletGeneratorFactory.h"

#include "FWCore/PluginManager/interface/PluginFactory.h"
 
EDM_REGISTER_PLUGINFACTORY(HitQuadrupletGeneratorFactory,"HitQuadrupletGeneratorFactory");
